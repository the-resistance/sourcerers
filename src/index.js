import $ from 'jquery'
const handTrack = require('handtrackjs')
import fireball from './js/fireball'
import snowball from './js/snowball'
import lightball from './js/lightball'

const actions = [
  () => fireball(),
  () => snowball(),
  () => lightball()
]

const video = document.getElementById("rawvideo")
const overlayimage = document.getElementById("overlayimage")
const canvas = document.getElementById("canvas-video")
const composite = document.getElementById("composite")
const context = canvas.getContext("2d")
const toggleButton = document.getElementById("togglebutton")
const fullscreenButton = document.getElementById("fullscreenbutton")
const updateNote = document.getElementById("updatenote")
const scorebox = document.getElementById("scorebox")
const imageflip = document.getElementById("imageflip")

let isVideo = false
let model = null

const modelParams = {
  flipHorizontal: JSON.parse(localStorage.getItem('flip')) || true, // flip e.g for video
  maxNumBoxes: 20, // maximum number of boxes to detect
  iouThreshold: 0.2, // ioU threshold for non-max suppression
  scoreThreshold: localStorage.getItem('threshold') || 0.5, // confidence threshold for predictions.
  modelSize: 'large',
  modelType: 'ssd640fpnlite',
  imageScaleFactor: 1.0
}

const getMedia = async (constraints) => {
  let stream = null
  try {
    stream = await navigator.mediaDevices.getUserMedia(constraints)
  } catch(err) {
    console.error(err)
  }
}

scorebox.innerText = modelParams.scoreThreshold

$('#scoreThreshold').change(function(){
  const score = $(this).val() / 100
  modelParams.scoreThreshold = score
  scorebox.innerText = score
  localStorage.setItem('threshold', score)
  loadModel()
})

$('#overlay').change(function(){
  if (canvas.style.display === 'inherit') {
    canvas.style.display = ("none")
  }
  else {
    canvas.style.width = 'auto'
    canvas.style.height = video.offsetHeight
    canvas.style.display = ("inherit")
  }
  flipCanvas()
  loadModel()
})

$('#imageflip').on('change', function(){
  if ($(this).is(':checked')) {
    modelParams.flipHorizontal = true
  } else {
    modelParams.flipHorizontal = false
  }
  localStorage.setItem('flip', modelParams.flipHorizontal)
  flipCanvas()
  loadModel()
})

const flipCanvas = () => {
  if (modelParams.flipHorizontal === true) {
    imageflip.checked = true
    video.style.transform = ("scaleX(-1)")
  }
  else {
    imageflip.checked = false
    video.style.transform = ("scaleX(1)")
  }
}

flipCanvas()

const startVideo = async () => {
  await getMedia({
    audio: false,
    video: { width: 1280, height: 720 }
  })

  handTrack.startVideo(video).then(function (status) {
    if (status) {
      updateNote.innerText = "Video started. Now watching..."
      isVideo = true
      runDetection()
    } else {
      updateNote.innerText = "Please enable video device."
    }
  })
}

const toggleVideo = async () => {
  if (!isVideo) {
    updateNote.innerText = "Starting video..."
    await startVideo()
    video.style.height = null
  } else {
    updateNote.innerText = "Stopping video..."
    handTrack.stopVideo(video)
    isVideo = false
    updateNote.innerText = "Video stopped"
  }
}

toggleButton.addEventListener("click", function () {
  toggleVideo()
})

fullscreenButton.addEventListener("click", function () {
  composite.requestFullscreen()
})

const runDetection = () => {
  model.detect(video).then((predictions) => {
    global.predictions = predictions
    model.renderPredictions(predictions, canvas, context, video)
    if (isVideo) {
      requestAnimationFrame(runDetection)
    }
  })
}

const runDetectionImage = (img) => {
  model.detect(img).then((predictions) => {
    global.predictions = predictions
    model.renderPredictions(predictions, canvas, context, img)
  })
}

// Load the model.
let interval = null
const loadModel = () => {
  updateNote.innerText = "Loading..."
  toggleButton.disabled = true
  fullscreenButton.disabled = true
  clearInterval(interval)
  handTrack.load(modelParams).then((lmodel) => {
    model = lmodel
    updateNote.innerText = "Loaded AI!"
    runDetectionImage(overlayimage)
    interval = setInterval(monitorActions, 250)
    toggleButton.disabled = false
    fullscreenButton.disabled = false
  })
}

loadModel()

global.live = false
let currentAction = 0
let attempt = 0
const monitorActions = () => {
  if (global.predictions[1] !== undefined && global.predictions[1].class === 4 && global.live === false) {
    if (attempt <= 1) return attempt++
    live = true
    if (currentAction === actions.length) {
      currentAction = 0
    }
    actions[currentAction]()
    currentAction++
  }
  attempt = 0
}